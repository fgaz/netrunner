#include <algorithm>
#include <iostream>
#include <memory>
#include <string>
#include "../../src/TextBlock.h"

int main(void) {
    TextBlock jenny;
    size_t lines;
    if (jenny.getValue() != "") {
        std::cout << "FAIL Start: " << jenny.getValue() << std::endl;
    }
    lines = jenny.setValue("Everybody mad at the rocks that I wear");
    if (jenny.getValue() != "Everybody mad at the rocks that I wear") {
        std::cout << "FAIL SetSingle: " << lines << " " << jenny.getValue() << std::endl;
    }
    jenny.deleteAt(0, 0, 1);
    if (jenny.getValue() != "verybody mad at the rocks that I wear") {
        std::cout << "FAIL DelFirst: " << lines << " " << jenny.getValue() << std::endl;
    }
    jenny.deleteAt(jenny.getValue().size() -1, 0, 1);
    if (jenny.getValue() != "verybody mad at the rocks that I wea") {
        std::cout << "FAIL DelLast: " << lines << " " << jenny.getValue() << std::endl;
    }
    lines = jenny.setValue("Men go working\rSome go stealing");
    if (jenny.getValue() != "Men go working\rSome go stealing") {
        std::cout << "FAIL SetMutli: " << lines << " " << jenny.getValue() << std::endl;
    }
    lines = jenny.setValue("Men go working\rSome go stealing\r");
    if (jenny.getValue() != "Men go working\rSome go stealing\r") {
        std::cout << "FAIL SetMutliTrailing: " << lines << " [" << jenny.getValue() << "]" << std::endl;
    }
    lines = jenny.setValue("");
    if (jenny.getValue() != "") {
        std::cout << "FAIL SetClear: " << lines << " [" << jenny.getValue() << "]" << std::endl;
    }
    jenny.insertAt("r", 0, 0);
    if (jenny.getValue() != "r") {
        std::cout << "FAIL InsertChar: " << lines << " " << jenny.getValue() << std::endl;
    }
    jenny.insertAt("cha", 0, 0);
    if (jenny.getValue() != "char") {
        std::cout << "FAIL InsertString: " << lines << " " << jenny.getValue() << std::endl;
    }
    jenny.insertAt("s", jenny.getValue().size(), 0);
    if (jenny.getValue() != "chars") {
        std::cout << "FAIL AppendChar: " << lines << " " << jenny.getValue() << std::endl;
    }
    jenny.insertAt("tring", jenny.getValue().size(), 0);
    if (jenny.getValue() != "charstring") {
        std::cout << "FAIL AppendString: " << lines << " " << jenny.getValue() << std::endl;
    }
    lines = jenny.setValue("135");
    if (jenny.getValue() != "135") {
        std::cout << "FAIL SetClear: " << lines << " [" << jenny.getValue() << "]" << std::endl;
    }
    jenny.insertAt("2", 1, 0);
    if (jenny.getValue() != "1235") {
        std::cout << "FAIL Insert2: " << lines << " " << jenny.getValue() << std::endl;
    }
    jenny.insertAt("4", 3, 0);
    if (jenny.getValue() != "12345") {
        std::cout << "FAIL Insert3: " << lines << " " << jenny.getValue() << std::endl;
    }
    jenny.deleteAt(3, 0, 1); // delete char we just added
    if (jenny.getValue() != "1235") {
        std::cout << "FAIL Delete3: " << lines << " " << jenny.getValue() << std::endl;
    }
    jenny.deleteAt(1, 0, 1); // delete char we just added
    if (jenny.getValue() != "135") {
        std::cout << "FAIL Delete1: " << lines << " " << jenny.getValue() << std::endl;
    }
    lines = jenny.setValue("asdf");
    if (jenny.getValue() != "asdf") {
        std::cout << "FAIL SetClear: " << lines << " [" << jenny.getValue() << "]" << std::endl;
    }
    jenny.deleteAt(2, 0, 1); // delete char we just added
    if (jenny.getValue() != "asf") {
        std::cout << "FAIL Delete2: " << lines << " " << jenny.getValue() << std::endl;
    }
    lines = jenny.setValue("\r");
    if (jenny.getValue() != "\r") {
        std::cout << "FAIL SetNewline: " << lines << " [" << jenny.getValue() << "]" << std::endl;
    }
    jenny.deleteAt(0, 0, 1); // delete char we just added
    if (jenny.getValue() != "") {
        std::cout << "FAIL DeleteLine: " << lines << " [" << jenny.getValue() << "]" << std::endl;
    }
    lines = jenny.setValue("zxcv\rzxcv\r");
    //jenny.print();
    if (jenny.getValue() != "zxcv\rzxcv\r") {
        std::cout << "FAIL Set2Newline: " << lines << " [" << jenny.getValue() << "]" << std::endl;
    }
    jenny.deleteAt(0, 2, 1); // delete char last trailing slash
    if (jenny.getValue() != "zxcv\rzxcv") {
        std::cout << "FAIL DeleteTrailingLine: " << lines << " [" << jenny.getValue() << "]" << std::endl;
    }
    jenny.deleteAt(3, 1, 1); // delete char last trailing slash
    if (jenny.getValue() != "zxcv\rzxc") {
        std::cout << "FAIL Delete3: " << lines << " [" << jenny.getValue() << "]" << std::endl;
    }
    jenny.deleteAt(2, 1, 1); // delete char last trailing slash
    if (jenny.getValue() != "zxcv\rzx") {
        std::cout << "FAIL Delete2: " << lines << " [" << jenny.getValue() << "]" << std::endl;
    }
    jenny.deleteAt(1, 1, 1); // delete char last trailing slash
    if (jenny.getValue() != "zxcv\rz") {
        std::cout << "FAIL Delete1: " << lines << " [" << jenny.getValue() << "]" << std::endl;
    }
    jenny.deleteAt(0, 1, 1); // delete char last trailing slash
    if (jenny.getValue() != "zxcv\r") {
        std::cout << "FAIL Delete0: " << lines << " [" << jenny.getValue() << "]" << std::endl;
    }
    return 0;
}
