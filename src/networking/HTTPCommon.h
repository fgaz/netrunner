#ifndef HTTPCOMMON_H
#define HTTPCOMMON_H
enum class Version {
    HTTP10,
    HTTP11
};

enum class Method {
    GET,
    POST
};
#endif
