#ifndef ENVIRONMENT_H
#define ENVIRONMENT_H

#include <string>

class Environment {
public:
	static void init();

	static std::string getResourceDir();

	static void setResourceDir();
private:
	static std::string resourceDir;
};
#endif
