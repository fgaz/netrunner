#ifndef PATH_H
#define PATH_H

#include <string>

class Path {
public:
	static bool directoryExists(const std::string &path);

	static std::string fromUnixPath(const std::string &path);
};

#endif
