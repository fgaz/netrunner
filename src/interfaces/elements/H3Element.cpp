#include "H3Element.h"

std::unique_ptr<Component> H3Element::renderer(const ElementRenderRequest &request) {
    TextNode *textNode = dynamic_cast<TextNode*>(request.node.get());
    if (textNode) {
        return std::make_unique<TextComponent>(textNode->text, 0, 0, 14, true, 0x000000FF, request.parentComponent->win->windowWidth, request.parentComponent->win->windowHeight); // Should be 14.04pt
    }
    return nullptr;
}
