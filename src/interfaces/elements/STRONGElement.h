#ifndef STRONGELEMENT_H
#define STRONGELEMENT_H

#include "Element.h"
#include "../components/Component.h"
#include "../components/TextComponent.h"
#include "../../parsers/markup/TextNode.h"

class STRONGElement : public Element {
public:
    STRONGElement();
    virtual std::unique_ptr<Component> renderer(const ElementRenderRequest &request);
};

#endif
