#ifndef AELEMENT_H
#define AELEMENT_H

#include "Element.h"
#include "../components/Component.h"
#include "../components/TextComponent.h"
#include "../../parsers/markup/TextNode.h"

void navTo(std::string url); // forward declaration

class AElement : public Element {
public:
    AElement();
    virtual std::unique_ptr<Component> renderer(const ElementRenderRequest &request);
};

#endif
