#include "SPANElement.h"

SPANElement::SPANElement() {
    isInline = true;
}

std::unique_ptr<Component> SPANElement::renderer(const ElementRenderRequest &request) {
    TextNode *textNode = dynamic_cast<TextNode*>(request.node.get());
    if (textNode) {
        // only create a dummy component if we have one child, why?
        if (request.node->parent->children.size() == 1) {
            std::unique_ptr<Component> component = std::make_unique<TextComponent>(textNode->text, 0, 0, 12, false, 0x000000FF, request.parentComponent->win->windowWidth, request.parentComponent->win->windowHeight);
            return component;
        }
    }
    return nullptr;
}
