#include "TagNode.h"

TagNode::TagNode() : Node(NodeType::TAG) {
}

// iterator down and extract any src attributes
std::vector<std::string> TagNode::getSourceList() {
    std::vector<std::string> returnVec;
    
    auto propIter = properties.find("src");
    if (propIter != properties.end()) {
        returnVec.push_back(propIter->second);
    }

    for (std::shared_ptr<Node>& child : children) {
        auto childSrcs = child->getSourceList();
        returnVec.insert(returnVec.end(),
                         childSrcs.begin(),
                         childSrcs.end());
    }
    
    return returnVec;
}

std::unique_ptr<std::pair<std::string, std::string>> TagNode::getTagNodeNameValue() {
    auto namePropIter = this->properties.find("name");
    if (namePropIter == this->properties.end()) {
        return nullptr;
    }
    auto valuePropIter = this->properties.find("value");
    if (valuePropIter == this->properties.end()) {
        return nullptr;
    }
    return std::make_unique<std::pair<std::string, std::string>>(namePropIter->second, valuePropIter->second);
}
