#ifndef TAGNODE_H
#define TAGNODE_H

#include "Node.h"
#include <map>

class TagNode : public Node {
public:
    TagNode();
    std::string tag;
    std::map<std::string, std::string> properties;

    std::vector<std::string> getSourceList() override;
    std::unique_ptr<std::pair<std::string, std::string>> getTagNodeNameValue();
};

#endif
